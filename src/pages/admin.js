// Initialize the CMS object

import { useEffect } from 'react';

// Now the registry is available via the CMS object.
// CMS.registerPreviewTemplate('my-template', MyTemplate)
export default () => {

    useEffect(() => {
        const CMS = require('netlify-cms-app')
        CMS.init()
    })

    return null;
};